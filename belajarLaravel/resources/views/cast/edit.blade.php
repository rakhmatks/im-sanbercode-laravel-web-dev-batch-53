@extends('layouts.master')

@section('judul','Edit Data Pemain Film')

@section('content')
<form action="/cast/{{$castById->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value={{$castById->nama}}>
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="number" min="0" class="form-control @error('umur') is-invalid @enderror" name="umur" value={{$castById->umur}}>
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea name="biodata" class="form-control @error('biodata') is-invalid @enderror" cols="30" rows="10">{{$castById->biodata}}</textarea>
    </div>
    @error('biodata')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    
@endsection