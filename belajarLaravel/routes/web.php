<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\bioController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homeController::class,"index"]);

Route::get('/register', [bioController::class,"daftar"]);

Route::post('/welcome', [bioController::class,"mulai"]);

Route::get('/table', function(){
    return view('pages.table');
});

Route::get('/data-table', function(){
    return view('pages.data-table');
});

// CRUD Cast

// C -> Create Data
// mengarahkan ke halaman form untuk menambah data Cast
Route::get('/cast/create',[CastController::class,"create"]);

// memasukan data cast ke dalam database
Route::post('/cast',[CastController::class,"store"]);

// R - Read Data
// menampilkan seluruh data cast
Route::get('/cast',[CastController::class,"index"]);

// menampilkan data cast berdasarkan id
Route::get('/cast/{id}',[CastController::class,"show"]);

// U - Update Data
// mengarahkan ke halaman update cast
Route::get('/cast/{id}/edit',[CastController::class,"edit"]);

//Mengubah data ke database berdasarkan Id
Route::put('/cast/{id}',[CastController::class,"update"]);

// D - Delete Data
// menghapus data dari database berdasarkan Id
Route::delete('/cast/{id}',[CastController::class,"destroy"]);