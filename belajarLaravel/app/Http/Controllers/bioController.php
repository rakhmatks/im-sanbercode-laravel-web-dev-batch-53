<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class bioController extends Controller
{
    public function daftar(){
        return view("pages.form");
    }

    public function mulai(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('pages.welcome',
        ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
