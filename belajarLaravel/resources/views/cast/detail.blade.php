@extends('layouts.master')

@section('judul','Detail Pemain Film')

@section('content')
    <h1 style="background-color:MediumSeaGreen;color:white"><b>{{$castById->nama}}</b> , usia {{$castById->umur}} tahun </h1>
    <p>Berasal dari {{$castById->biodata}}</p>
@endsection
