<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        //Validasi
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'biodata' => 'required',
        ]);

        //Insert Data
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'biodata' => $request['biodata'],
        ]);

        //Redirect ke halaman cast /cast
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        
        return view('cast.index',["cast" => $cast]);
    }

    public function show($id){
        $castById = DB::table('cast')->find($id);

        return view('cast.detail',["castById" => $castById]);
    }

    public function edit($id){
        $castById = DB::table('cast')->find($id);

        return view('cast.edit',["castById" => $castById]);
    }

    public function update($id, Request $request){
        //Validasi
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'biodata' => 'required',
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'biodata' => $request['biodata'],
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')
        ->where('id', $id)
        ->delete();
        return redirect('/cast');
    }
}
