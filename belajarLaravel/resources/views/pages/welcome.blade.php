@extends('layouts.master')

@section('judul','Welcome')

@section('content')
    <h1>&#127881 SELAMAT DATANG ! &#127881</h1>
    
    <h2>Terima kasih <font color="blue"> {{ $namaDepan }} {{ $namaBelakang }} </font> telah bergabung di SanberBook.<br>
        Social Media kita bersama!</h2>
@endsection
