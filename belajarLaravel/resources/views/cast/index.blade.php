@extends('layouts.master')

@section('judul','Pemain Film')

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Tambah Pemain Film</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pemain Film</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
    @forelse ( $cast as $keys => $item )
    <tr>
        <th scope="row">{{$keys + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
            </form>
        </td>
    </tr>  
    @empty
    <tr>
        <td>Daftar Pemain Film Kosong</td>
    </tr>
    @endforelse
          
    </tbody>
  </table>
@endsection
